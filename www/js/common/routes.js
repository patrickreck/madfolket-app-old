MadfolketApp.config(['$stateProvider', '$urlRouterProvider', 
	function($stateProvider, $urlRouterProvider) {

		$stateProvider

		.state('tab', {
			url: "/tab",
			abstract: true,
			templateUrl: "templates/tabs.html"
		})

		.state('tab.recipes', {
			url: '/recipes',
			views: {
				'tab-recipes': {
					templateUrl: 'js/modules/recipes/partials/recipes.html',
					controller: 'RecipesController',
		            resolve: {
		                recipes : ['RecipesService', '$q', function(RecipesService, $q) {
		                    var defer = $q.defer();

		                    RecipesService.getRecipes().then(function(response) {
		                        defer.resolve(response.data);
		                    });

		                    return defer.promise;
		                }]
		            },
				}
			}
		})

		.state('tab.recipe', {
			url: '/recipes/:recipeId',
			views: {
				'tab-recipes': {
					templateUrl: 'js/modules/recipes/partials/recipe.html',
					controller: 'RecipeController',
		            resolve: {
		                data : ['RecipesService', '$q', '$stateParams', function(RecipesService, $q, $stateParams) {
		                    var defer = $q.defer();

		                    RecipesService.getRecipe($stateParams.recipeId).then(function(response) {
		                        defer.resolve(response.data);
		                    });

		                    return defer.promise;
		                }]
		            },

				}
			}
		})

		.state('tab.shopping-lists', {
			url: '/shopping-lists',
			views: {
				'tab-shoppinglists': {
		            templateUrl: "js/modules/shopping-list/partials/shopping-lists.html",
					controller: 'ShoppingListsController',
		            resolve: {

		                shoppingLists : ['ShoppingListService', '$q', function(ShoppingListService, $q) {
		                    var defer = $q.defer();

		                    console.log('getting');

		                    ShoppingListService.getShoppingLists().then(function(response) {
		                        defer.resolve(response.data);
		                    });

		                    return defer.promise;
		                }]
		            },
		            
				}
			}
		})

		.state('tab.shopping-list', {
			url: '/shopping-lists/:shoppingListId',
			views: {
				'tab-shoppinglists': {
		            templateUrl: "js/modules/shopping-list/partials/shopping-list.html",
					controller: 'ShoppingListController',
		            resolve: {
		                shoppingList: ['ShoppingListService', '$q', '$stateParams', function(ShoppingListService, $q, $stateParams) {
		                    var defer = $q.defer();

		                    ShoppingListService.getShoppingList($stateParams.shoppingListId).then(function(response) {
		                    console.log('aa');

		                        defer.resolve(response.data);
		                    });

		                    return defer.promise;
		                }]
		            },
				}
			}
		})

		.state('user', {
			url: '/user',
			template: 'user',
		});

		$urlRouterProvider.otherwise('/tab/recipes');
	}
]);