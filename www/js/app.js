var MadfolketApp = angular.module('MadfolketApp', [
	'ionic', 
	'http-auth-interceptor',
	'ngCordova',
	'angular-data.DSCacheFactory',
	'facebook',

	'RecipesModule',
	'ShoppingListModule',
	'AuthenticationModule',
])

.config(function($ionicConfigProvider) {
	$ionicConfigProvider.tabs.style('standard');
	$ionicConfigProvider.tabs.position('bottom');
})

.config(function(FacebookProvider) {
	FacebookProvider.init('1488234771462602');
})

.run(function($ionicPlatform, $rootScope, $cordovaStatusbar, $ionicLoading, $cordovaSplashscreen, $interval, DSCacheFactory, AuthenticationService) {

	$rootScope.api = 'http://172.20.10.7/madfolket-server/public';
	$rootScope.api = 'http://localhost/madfolket-server/public';
	$rootScope.api = 'http://madfolket-server.herokuapp.com/public';
	$rootScope.api = 'http://test.madfolket.dk/madfolket-server/public';

	AuthenticationService.updateLoggedIn();

	$rootScope.getSlugified = function(string) {
		return $filter('slugify')(string);
	}

	$rootScope.isLoggedIn = function() {
		return AuthenticationService.isLoggedIn();
	}

	$rootScope.getUser = function() {
		return AuthenticationService.getUser();
	}

	/* Cache
	DSCacheFactory('defaultCache', {
		maxAge: 300000,
		cacheFlushInterval: 6000000,
		deleteOnExpire: 'aggressive'
	});
	$http.defaults.cache = DSCacheFactory.get('defaultCache');
	*/

	$rootScope.$on('$stateChangeStart', function() {
		$ionicLoading.show({
			template: '<i class="icon ion-loading-c loadinga"></i>',
			animation: 'fade-in',
			showBackdrop: true,
			showDelay: 300
		});
	})

	$rootScope.$on('$stateChangeSuccess', function() {
		$ionicLoading.hide()
	})

	$ionicPlatform.ready(function() {

		if(window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

			$interval(function() {
				$cordovaSplashscreen.hide();
			}, 2000, 1)
		}

		if(window.StatusBar) {
			StatusBar.styleDefault();
			$cordovaStatusbar.style(2);
		}
	});




})



.config(function($httpProvider) {
  $httpProvider.interceptors.push(function($rootScope) {
    return {
      request: function(config) {
        //$rootScope.$broadcast('loading:show')
        return config
      },
      response: function(response) {
        //$rootScope.$broadcast('loading:hide')
        return response
      }
    }
  })
})

