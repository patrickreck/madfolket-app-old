AuthenticationModule.service('AuthenticationService', ['$rootScope', '$http', 'AuthenticationFacebookService',
	function($rootScope, $http, AuthenticationFacebookService) {

		var self = this;

		this.loggedIn = false;
		this.user = null;

		this.login = function(token) {

			return $http.post($rootScope.api + '/authentication/login', {token: token}).then(function(response) {
				console.log(response);
				self.loggedIn = true;
				self.user = response.data;
				console.debug(self.loggedIn);
			});

		};

		this.logout = function() {
			$http.post($rootScope.api + '/authentication/logout').then(function(response) {
				AuthenticationFacebookService.logout();
				self.loggedIn = false;
			});
		};


	    this.updateLoggedIn = function() {



			$http.get($rootScope.api + '/authentication/check-login').then(function(response) {
				console.log('You are logged in');
				self.loggedIn = true;
				self.user = response.data;
			}, function(response) {
				console.log('You are not logged in');
				self.loggedIn = false;
			});

	    };

	    this.isLoggedIn = function() {
	    	return self.loggedIn;
	    }

	    this.getUser = function() {
	    	return self.user;
	    }


	}
]);