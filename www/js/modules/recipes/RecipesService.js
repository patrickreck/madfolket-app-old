RecipesModule.service('RecipesService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getRecipes = function() {
			return $http.get($rootScope.api + '/recipes');
		}

		this.searchRecipes = function(filters) {
			return $http.post($rootScope.api + '/recipes/search-recipes', filters);
		}

		this.getRecipe = function(recipeId) {
			return $http.get($rootScope.api + '/recipes/recipe/' + recipeId);
		}

		this.getOrigins = function() {
			return $http.get($rootScope.api + '/origins');
		}

		this.getTags = function() {
			return $http.get($rootScope.api + '/tags');
		}

		this.getCategories = function() {
			return $http.get($rootScope.api + '/categories');
		}

		this.getIngredients = function(ingredient) {
			return $http.get($rootScope.api + '/ingredients/' + ingredient);
		}

		this.getUnits = function() {
			return $http.get($rootScope.api + '/units');
		}

		this.saveRecipe = function(recipe) {
			return $http.post($rootScope.api + '/recipes/save', { 
				name: recipe.name,
				persons: recipe.persons,
				introduction: recipe.introduction,
				hours: recipe.hours,
				minutes: recipe.minutes,
				freezable: recipe.freezable,
				origin: recipe.origin,
				category: recipe.category,
				tags: recipe.tags,
				ingredients: recipe.ingredients,
				steps: recipe.steps,
				images: recipe.images
			});
		}

		this.rateRecipe = function(recipe, rating) {
			return $http.post($rootScope.api + '/recipes/rate', {
				recipe_id: recipe.id,
				rating: rating
			});
		}

		this.addComment = function(recipe, comment) {
			return $http.post($rootScope.api + '/recipes/save-comment', {
				recipe_id: recipe.id,
				comment: comment
			});
		}

		this.getMoreComments = function(recipeId, skip) {
			return $http.get($rootScope.api + '/recipes/more/' + recipeId + '/' + skip);
		}

	}

]);