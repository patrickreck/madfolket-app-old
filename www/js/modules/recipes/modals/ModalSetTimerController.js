RecipesModule.controller('ModalSetTimerController', function($scope, $modalInstance, data) {

	$scope.timer = {
		hours: 0,
		minutes: 0,
		seconds: 0
	};

	if (data.timer) {
		$scope.timer = $scope.convertSecondsToTime(data.timer);
		$scope.predefined = true;
	}

	$scope.addTimer = function() {
		var timer = $scope.timer;
		if (!angular.isNumber(timer.hours) || !angular.isNumber(timer.minutes) || !angular.isNumber(timer.seconds))
			return;

		$modalInstance.close({timer: $scope.calculateSeconds(), index: data.index});
	}

	$scope.calculateSeconds = function() {
		var timer = $scope.timer;
		var seconds = 0;
		seconds = seconds + timer.hours * 60 * 60;
		seconds = seconds + timer.minutes * 60;
		seconds = seconds + timer.seconds;

		return seconds;
	}



	$scope.removeTimer = function() {
		$modalInstance.close({timer: null, index: data.index});
	}

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	}

})