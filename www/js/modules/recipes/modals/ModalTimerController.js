RecipesModule.controller('ModalTimerController', function($scope, $modalInstance, seconds, $interval, ngAudio) {

	$scope.timer = $scope.convertSecondsToTime(seconds);

	var sound = ngAudio.load("sounds/alarm.mp3");

	var originalSeconds = seconds;
	$scope.seconds = 120;
	
	$scope.running = false;
	var intervalTimer;

	$scope.playingSound = false;

	var updateTimer = function() {
		$scope.seconds--;
		$scope.timer = $scope.convertSecondsToTime($scope.seconds);
		console.log($scope.seconds);

		if ($scope.seconds == 0) {
			$scope.running = false;
			$interval.cancel(intervalTimer);
			sound.play();
			$scope.playingSound = true;
		}
	}

	$scope.stopSound = function() {
		$scope.playingSound = false;
		sound.stop();
	}

	$scope.startTimer = function() {
		$scope.running = true;
		intervalTimer = $interval(updateTimer, 50);		
	}

	$scope.pauseTimer = function() {
		$scope.running = false;
		$interval.cancel(intervalTimer);
	}

	$scope.resetTimer = function() {
		$scope.stopSound();
		$scope.seconds = originalSeconds;
		$scope.timer = $scope.convertSecondsToTime(seconds);
		$interval.cancel(intervalTimer);
	}


	$scope.cancel = function() {
		$scope.pauseTimer();
		$scope.stopSound();
		$modalInstance.dismiss('cancel');
	}


})