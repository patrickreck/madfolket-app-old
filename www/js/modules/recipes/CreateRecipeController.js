RecipesModule.controller('CreateRecipeController', function($scope, $sce, RecipesService, $modal, orderByFilter, $upload, $state) {

	/* Recipe object to be submitted to the server, simulates actual recipe object */
	$scope.recipe = {
		freezable: false,
		persons: 4,
		steps: [
		],
		ingredients: [
		],
		tags: [
		],
		images: [		
		],
	};

	/* Steps */
	$scope.steps = [
		{ name: 'Basale informationer', icon: 'ion-clipboard', tips: 'For at få flest mulige besøgende på din opskrift, bør du lave en fangende introduktion. <br/><br/>Derudover anbefaler vi at vælge et præcist navn.' },
		{ name: 'Kategorisering', icon: 'ion-earth', tips: 'For at kunne kategorisere din opskrift bedst muligt, skal du angive oprindelseslandet og en række kategorier.' },
		{ name: 'Ingredienser', icon: 'ion-pizza', tips: 'Her skal du indtaste de ingredienser der er brug for i din opskrift.<br/><br/>Du kan lave <b>opdelinger</b> til f.eks. Kød og Sovs eller Kage og Glasur.' },
		{ name: 'Vejledning', icon: 'ion-ios7-browsers', tips: 'For at guide dine læsere bedst igennem din opskrift, skal du indtaste den i <b>skridt</b>.<br/><br/>Ud for hvert skridt kan du aktivere et <b>æggeur</b> der gør det lættere for din læser at tage tid. Det kan f.eks. være hvis din kage skal bages i 20 minutter.' },
		{ name: 'Billeder', icon: 'ion-image', tips: 'Din opskrift vil få flere besøgende hvis der er billeder af resultatet. Det er dog <b>valgfrit</b> at ligge billeder op. <br/><br/> Når du har lagt mere end 1 billede op, kan du vælge hovedbilledet (det billede der vil blive vist først) ved at <b>klikke på miniaturen</b>.' }
	];
	$scope.currentStep = 0;

	$scope.nextStep = function() {
		if ($scope.currentStep !== $scope.steps.length - 1)
			$scope.currentStep++;
	}
	$scope.previousStep = function() {
		if ($scope.currentStep !== 0)
			$scope.currentStep--;
	}
    $scope.setStep = function(step) {
		$scope.currentStep = step;
	}

	/* ------ STEP 00 ------ */
	$scope.persons = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];

	/* ------ STEP 01 ------ */

	/* Get origins and handle */
	$scope.origins = null;
	RecipesService.getOrigins().then(function(response) {
		$scope.origins = response.data;
		$scope.recipe.origin = $scope.origins[42];
	});


	/* Get categories and handle */
	$scope.categories = null;
	RecipesService.getCategories().then(function(response) {
		$scope.categories = response.data;
		$scope.recipe.category = $scope.categories[0];
	})

	/* Get tags and handle */
	$scope.tags = [];

	var reorderTags = function() {	$scope.tags = orderByFilter($scope.tags, 'name'); }

	RecipesService.getTags().then(function(response) {

		angular.forEach(response.data, function(category) {

			angular.forEach(category.tags, function(tag) {
				tag.type = category.name;
				$scope.tags.push(tag);
			})
		});

		reorderTags();

		$scope.currentTag = $scope.tags[0];
	})

	$scope.addTag = function() {
		$scope.recipe.tags.push($scope.currentTag);

		var index = $scope.tags.indexOf($scope.currentTag);

		$scope.tags.splice(index, 1);

		reorderTags();

		$scope.currentTag = $scope.tags[0];
	}

	$scope.removeTag = function(tag) {
		$scope.tags.push(tag);
	
		var index = $scope.recipe.tags.indexOf(tag);
		$scope.recipe.tags.splice(index, 1);

		reorderTags();
	}
	/* ------/ STEP 01 /------ */



	/* ------ STEP 02 ------ */

	/* Handle ingredients */
	$scope.currentIngredient = {};

	$scope.chosenIngredient = undefined; 
	$scope.getIngredients = function(val) {
		return RecipesService.getIngredients(val).then(function(response) {
			return response.data;
		});
	}
	$scope.$watch('chosenIngredient', function(ingredient) {
		$scope.currentIngredient.name = ingredient; 
	});


	$scope.units = undefined;
	RecipesService.getUnits().then(function(response) {
		$scope.units = response.data;
		$scope.currentIngredient.unit = $scope.units[0];
	});

	$scope.addIngredient = function() {
		$scope.recipe.ingredients.push(angular.copy($scope.currentIngredient));

		$scope.currentIngredient.unit = $scope.units[0];
		$scope.currentIngredient.amount = '';
		$scope.chosenIngredient = undefined;
	}

	$scope.removeIngredient = function(index) {
		$scope.recipe.ingredients.splice(index, 1);
	}
	/* ------/ STEP 02 /------ */



	/* ------ STEP 03 ------ */


	/* The steps to create the recipe */
	$scope.newStep = {};
	$scope.addStep = function() {
		$scope.recipe.steps.push($scope.newStep);
		$scope.newStep = {};
	}

	$scope.removeStep = function(index) {
		$scope.recipe.steps.splice(index, 1);
	}

	/* Set timer modal */

	$scope.setTimerModal = function (index) {

		var modalInstance = $modal.open({
			templateUrl: 'js/modules/recipes/modals/partials/modal-set-timer.html',
			controller: 'ModalSetTimerController',
			size: 'md',
			resolve: {
				data: function () {
					return {timer: $scope.recipe.steps[index].timer, index: index};						
				}
			}

		});

		modalInstance.result.then(function (data) {

			$scope.recipe.steps[data.index].timer = data.timer;
		
		}, function () {
			console.log('Modal dismissed at: ' + new Date());
		});
	};



	/* ------/ STEP 03 /------ */

	/* Upload */

	$scope.uploading = false;
	$scope.onFileSelect = function($files) {

		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];
			$scope.upload = $upload.upload({
				url: $scope.api + '/recipes/save-image',
				method: 'POST',
				file: file,
			}).progress(function(evt) {
				var percent = parseInt(100.0 * evt.loaded / evt.total);
				if (percent !== 100) {
					$scope.uploading = true;
				}
				else {
					$scope.uploading = false;
				}
				console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
			}).success(function(data, status, headers, config) {
				data.hovered = false;
				console.log(data);
				$scope.recipe.images.push(data);
				$scope.changeMainImage($scope.recipe.images[0]);
				$scope.uploadError = undefined;
			}).error(function(data) {
				console.log(data);
				$scope.uploadError = data;
			});
		}
	};

	$scope.changeMainImage = function(image) {
		angular.forEach($scope.recipe.images, function(loopedImage) {
			loopedImage.main = false;
		});

		image.main = true;

		$scope.mainImage = image;
	}

	$scope.removeImage = function(image) {
		console.log('removing');

		var index = $scope.recipe.images.indexOf(image);

		$scope.recipe.images.splice(index, 1);

		if ($scope.recipe.images.length > 0) {
			if ($scope.mainImage == image) {
				$scope.mainImage = $scope.recipe.images[0];
			}			
		}
		else {
			$scope.mainImage = undefined;
		}
	}

	$scope.imageToggleHover = function(image) {
		image.hovered = !image.hovered;
	}


	/* ------ SUBMIT THAT SHI ------ */

	$scope.submitRecipe = function() {

		var recipe = angular.copy($scope.recipe);
		recipe.category = recipe.category.id;
		recipe.origin = recipe.origin.id;

		var temporaryTags = [];
		angular.forEach(recipe.tags, function(tag) {
			temporaryTags.push(tag.id);
		});
		recipe.tags = temporaryTags;

		var temporaryIngredients = [];
		angular.forEach(recipe.ingredients, function(ingredient) {
			temporaryIngredients.push({name: ingredient.name, amount: ingredient.amount, unit: ingredient.unit.id})
		});
		recipe.ingredients = temporaryIngredients;

		var temporaryImages = [];
		angular.forEach(recipe.images, function(image) {
			temporaryImages.push({ path: image.path, main: image.main});
		});
		recipe.images = temporaryImages;

		var temporarySteps = [];
		angular.forEach(recipe.steps, function(step) {
			if (step.timer != undefined && step.timer > 0)
				temporarySteps.push(step);
			else
				temporarySteps.push({ description: step.description });
		});
		recipe.steps = temporarySteps;


		RecipesService.saveRecipe(recipe).then(function(response) {
			console.log(response.data);
			var recipe = response.data;
			$state.go('recipe', {recipeId: recipe.id, recipeName: $scope.getSlugified(recipe.name)});
		});



	}


})







