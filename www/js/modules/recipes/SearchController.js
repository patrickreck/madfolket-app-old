RecipesModule.controller('SearchController', function($scope, recipes) {

    $scope.recipes = recipes;

    $scope.toggleActive = function(recipe) {
        recipe.active = !recipe.active;
    }

})