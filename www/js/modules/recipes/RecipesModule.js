var RecipesModule = angular.module('RecipesModule', [])

.run(['$rootScope', function($rootScope) {

	$rootScope.convertSecondsToTime = function(seconds) {
	    return {
			hours: Math.floor(seconds / 3600),
			minutes: parseInt(seconds / 60) % 60,
			seconds: parseInt(seconds % 60, 10)
	    }
	}

}]);