RecipesModule.controller('RecipesController', function($scope, recipes, $state, RecipesService) {

console.log(window.location.href);

	$scope.recipes = recipes;
	$scope.filters = {categories: [], origins: []};

	$scope.categories = [];
	RecipesService.getCategories().then(function(response) {
		$scope.categories = response.data;
	});

	$scope.origins = [];
	RecipesService.getOrigins().then(function(response) {
		$scope.origins = response.data;
		$scope.origin = $scope.origins[42];
	});

	$scope.ingredients = [];

$scope.$watch('ingredient.selected', function(ingredient) {

	RecipesService.getIngredients(ingredient).then(function(response) {
		console.log(response.data);
		$scope.ingredients = response.data;
	});
});



	$scope.toggleCategory = function(category) {
		var index = $scope.filters.categories.indexOf(category.id);

		if (index > -1)
			$scope.filters.categories.splice(index, 1);
		else
			$scope.filters.categories.push(category.id);

		$scope.updateFilters();
	}

	$scope.toggleOrigin = function(origin) {
		var index = $scope.filters.origins.indexOf(origin.id);

		if (index > -1)
			$scope.filters.origins.splice(index, 1);
		else
			$scope.filters.origins.push(origin.id);

		$scope.updateFilters();
	}

	$scope.updateFilters = function() {

		RecipesService.searchRecipes($scope.filters).then(function(response) {
			console.log(response);
			$scope.recipes = response.data;
		});
	}

})