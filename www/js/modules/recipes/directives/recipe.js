RecipesModule.directive('recipe', [function(){

	return {
		
		scope: {
		 	recipe: '=',
		},
		
		restrict: 'E',
		templateUrl: 'js/modules/recipes/directives/templates/recipe.html',

		link: function($scope) {
		    
		}
	};
}]);