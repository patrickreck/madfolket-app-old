RecipesModule.directive('recipeBig', [function(){

	return {
		
		scope: {
		 	recipe: '=',
		},
		
		restrict: 'E',
		templateUrl: 'js/modules/recipes/directives/templates/recipe-big.html',

		link: function($scope) {
		    
		}
	};
}]);