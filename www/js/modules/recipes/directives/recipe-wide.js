RecipesModule.directive('recipeWide', [function(){

	return {
		
		scope: {
		 	recipe: '=',
		},
		
		restrict: 'E',
		templateUrl: 'js/modules/recipes/directives/templates/recipe-wide.html',

		link: function($scope) {
		    
		}
	};
}]);