CookbooksModule.controller('CookbooksController', function($scope) {

	$scope.cookbooks = [
		{ id: 1, description: 'En lækker pizza fra det sydlige Italien', active: false, author: 'Patrick Reck', title: 'Mad til gæster', time: '25 min', stars: 3, picture: 'http://pinnest.net/newpinnest/wp-content/uploads/2013/08/137724465553a2c.jpg' },
		{ id: 2, description: 'Sushi som var det var en Sushi-mester!', active: false, author: 'Nicklas Kevin Frank', title: 'Børnefødselsdage', time: '2 t 30 min', stars: 5, picture: 'http://www.madogbolig.dk/~/media/websites/madogbolig.dk/Website/Gourmet/gourmetmadpaabudget_p.ashx' },
		{ id: 3, description: 'Krabber fra Jensens Fiskerestaurant', active: false, author: 'Lisbeth Olesen', title: 'Italiens goder', time: '50 min', stars: 4, picture: 'http://multimedia.pol.dk/archive/00602/iStock_kammusling_r_602603a.jpg' },
		{ id: 4, description: 'Grillet mørbrad med hvidløg. Perfekt til sommeren', active: false, author: 'Anna Marie', title: 'Ost ost ost!', time: '1 t 15 min', stars: 1, picture: 'http://images.fyens.dk/67/914467_608_608_0_109_2126_1195.jpg' },

		{ id: 5, description: 'Pandekagerne som farmor lavede dem', active: false, author: 'Bob the Panmaker', title: 'Patrick har fødselsdag', time: '3 t', stars: 2, picture: 'http://www.pamelasproducts.com/wp-content/uploads/2012/09/Pancakes.jpg' },
		{ id: 6, description: 'En Kaj kage som alle kan være med på', active: false, author: 'Den skaldede kok', title: 'Mad ungerne kan lide', time: '30 min', stars: 1, picture: 'http://2.bp.blogspot.com/-3Cq9NXQ7lz8/Tc_bmG4eaQI/AAAAAAAAACk/1xRQ-q0EthE/s320/HPIM1644.JPG' },
		{ id: 7, description: 'Jødekager som kun Thomas kan lave dem', active: false, author: 'Thomazi Bornerup', title: 'Desertamums', time: '19 t 45 min', stars: 5, picture: 'http://www.dk-kogebogen.dk/billeder-opskrifter/billeder/19410/4_300.jpg' },
		{ id: 8, description: 'Citron fromages over alle citron fromager', active: false, author: 'Mikes mor', title: 'Dømt i gryderetten', time: '5 t 10 min', stars: 4, picture: 'http://www.madkogebogen.dk/billeder/citronfromage1_large.jpg' },
	];
    
    // Kun til demo af siden uden backend

    $scope.lastPictures = [];
    $scope.randomPicture = function() {

        var foundPicture = false;
        var picture = null;
        
        while (!foundPicture) {

            var tempPicture = $scope.getRandomPicture();

            var duplicate = false;

            angular.forEach($scope.lastPictures, function(lastPicture) {
               if (lastPicture == tempPicture) {
                   duplicate = true;
               } 
            });            
            
            if (!duplicate) {
                foundPicture = true;
                picture = tempPicture;
            }
        }
        
        if ($scope.lastPictures.length == 4) {
            $scope.lastPictures = [];            
        }
        
        $scope.lastPictures.push(picture);

        return picture;
    }
    
    $scope.getRandomPicture = function() {
        var picture = $scope.cookbooks[Math.floor(Math.random() * $scope.cookbooks.length)].picture;
        return picture;
    }
    
    
        angular.forEach($scope.cookbooks, function(cookbook) {
        cookbook.pictures = [];
        
        for (i = 0; i < 4; i++) { 
        
            cookbook.pictures.push($scope.randomPicture());
            
        }
         
    });
})