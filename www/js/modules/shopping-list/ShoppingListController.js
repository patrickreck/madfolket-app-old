ShoppingListModule.controller('ShoppingListController', function($scope, ShoppingListService, shoppingList) {

	$scope.shoppingList = shoppingList;
	console.log(shoppingList);
	$scope.doRefresh = function() {
		ShoppingListService.getShoppingList(shoppingList.id).then(function(response) {
			$scope.shoppingList = response.data;
			$scope.$broadcast('scroll.refreshComplete');
		});
	}

	$scope.changedIngredient = function(ingredient) {
		ShoppingListService.setPurchased(shoppingList.id, ingredient.id, ingredient.purchased);
	}

})