ShoppingListModule.service('ShoppingListService', ['$rootScope', '$http', 
	function($rootScope, $http) {

		this.getShoppingLists = function() {
			return $http.get($rootScope.api + '/shopping-lists');
		}

		this.getShoppingList = function(shoppingListId) {
			return $http.get($rootScope.api + '/shopping-lists/shopping-list/' + shoppingListId);
		}

		this.getNames = function() {
			return $http.get($rootScope.api + '/shopping-lists/names');
		}

		this.create = function(name) {
			return $http.post($rootScope.api + '/shopping-lists/save', {'name': name});
		}

		this.addRecipe = function(shoppingListId, recipeId, persons) {
			return $http.post($rootScope.api + '/shopping-lists/add-recipe', {
				shoppingList: shoppingListId,
				recipe: recipeId,
				persons: persons
			});
		}

		// IngredientEntry er ID´et på shopping_lists_ingredients.id, IKKE shopping_lists_ingredients.ingredient_id
		this.setPurchased = function(shoppingListId, ingredientEntry, purchased) {
			return $http.post($rootScope.api + '/shopping-lists/set-purchased', {
				shoppingList: shoppingListId,
				ingredient: ingredientEntry,
				purchased: purchased
			});			
		}

	}
]);