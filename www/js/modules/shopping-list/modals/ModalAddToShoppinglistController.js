ShoppingListModule.controller('ModalAddToShoppingListController', function($scope, $modalInstance, ShoppingListService, recipe) {

	$scope.recipe = recipe;

	$scope.shoppingListName = "";

	$scope.shoppingLists = [];
	var populateShoppingLists = function() {
		console.log('Going');
		ShoppingListService.getNames().then(function(response) {
			$scope.shoppingLists = response.data;
			console.log($scope.shoppingLists);
		});
	}
	populateShoppingLists();

	$scope.creating = false;
	$scope.createShoppingList = function() {
		$scope.creating = true;
		ShoppingListService.create($scope.shoppingListName).then(function(response) {

			ShoppingListService.addRecipe(response.data, recipe.id, recipe.persons).then(function(response) {
				$scope.creating = false;
				$modalInstance.close();
			});

		});
	}

	$scope.addToShoppingList = function() {
		$scope.creating = true;
		ShoppingListService.addRecipe($scope.chosenShoppingList.id, recipe.id, recipe.persons).then(function(response) {
			$scope.creating = false;
			$modalInstance.close();
		});		
	}

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	}

})