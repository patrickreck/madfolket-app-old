ShoppingListModule.controller('ShoppingListsController', function($scope, ShoppingListService, shoppingLists) {

	$scope.shoppingLists = shoppingLists;

	angular.forEach(shoppingLists, function(shoppingList) {
		
		var ingredients = [];
		
		angular.forEach(shoppingList.recipes, function(recipe) {
			angular.forEach(recipe.ingredients, function(ingredient) {
				ingredients.push(ingredient);
			});
		});

		shoppingList.ingredients = ingredients;
	});

})