NotificationsModule.controller('NotificationsController', function($scope, NotificationsService) {

    $scope.notifications = ""

    $scope.getNotifications = function(userId){
        NotificationsService.getNotifications(userId).then(function(response) {
            $scope.notifications = response.data;
        });
    }

});
