NotificationsModule.service('NotificationsService', ['$rootScope', '$http', 
	function($rootScope, $http) {
		
		this.getNotifications = function(userId) {
			return $http.get($rootScope.api + '/notifications/notifications/' + userId);
		}
	}
]);