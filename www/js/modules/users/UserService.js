UsersModule.service('UserService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getUser = function(user_id) {
			return $http.get($rootScope.api + '/users/user/' + user_id);
		}

		this.getBadges = function(user_id) {
			return $http.get($rootScope.api + '/users/badges/' + user_id);
		}

		this.getFeed = function(user_id) {
			return $http.get($rootScope.api + '/users/feed/' + user_id);
		}

	}

]);